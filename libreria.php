<?php 

function encabezado($titulo)
{
  $encabezado="<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Funciones y estiloss</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.5 -->
    <link rel='stylesheet' href='estilos/public/css/bootstrap.min.css'>
    <!-- Font Awesome -->
    <link rel='stylesheet' href='estilos/public/css/font-awesome.css'>
    <!-- Theme style -->
    <link rel='stylesheet' href='estilos/public/css/AdminLTE.min.css'>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel='stylesheet' href='estilos/public/css/_all-skins.min.css'>
    <link rel='apple-touch-icon' href='estilos/public/img/apple-touch-icon.png'>
    <link rel='shortcut icon' href='estilos/public/img/favicon.ico'>

    

  </head>
  <body class='hold-transition skin-blue-light sidebar-mini'>
    
      <header class=''>

    <nav class='navbar navbar-default'>
  <div class='container-fluid'>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class='navbar-header'>
      <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
        <span class='sr-only'>Toggle navigation</span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
      </button>
      <a class='navbar-brand' href='#'>Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
      <ul class='nav navbar-nav'>
        <li class='active'><a href='#'>Link <span class='sr-only'>(current)</span></a></li>
        <li><a href='#'>Link</a></li>
        <li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Dropdown <span class='caret'></span></a>
          <ul class='dropdown-menu'>
            <li><a href='#'>Action</a></li>
            <li><a href='#'>Another action</a></li>
            <li><a href='#'>Something else here</a></li>
            <li role='separator' class='divider'></li>
            <li><a href='#'>Separated link</a></li>
            <li role='separator' class='divider'></li>
            <li><a href='#'>One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <form class='navbar-form navbar-left'>
        <div class='form-group'>
          <input type='text' class='form-control' placeholder='Search'>
        </div>
        <button type='submit' class='btn btn-default'>Submit</button>
      </form>
      <ul class='nav navbar-nav navbar-right'>
        <li><a href='#'>Link</a></li>
        <li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Dropdown <span class='caret'></span></a>
          <ul class='dropdown-menu'>
            <li><a href='#'>Action</a></li>
            <li><a href='#'>Another action</a></li>
            <li><a href='#'>Something else here</a></li>
            <li role='separator' class='divider'></li>
            <li><a href='#'>Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
      </header>
      

";

 echo $encabezado;
}


function piedepagina($footer)
{
  $pie="


    <footer class='panel panel-default panel-footer footer navbar-fixed-bottom'>
        <div class='pull-right hidden-xs'>
          <b>Version</b> 1.0
        </div>
        <strong>$footer<a href='#'>David Ulloa</a>.</strong> Todos los derechos reservados.
    </footer>    
    <!-- jQuery -->
    <script src='estilos/public/js/jquery-3.1.1.min.js'></script>
    <!-- Bootstrap 3.3.5 -->
    <script src='estilos/public/js/bootstrap.min.js'></script>
    <!-- AdminLTE App -->
    <script src='estilos/public/js/app.min.js'></script>

   
  </body>
</html>


   ";
 echo $pie;
}
 ?>



